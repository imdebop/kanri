# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.

require 'java'
require 'jrubyfx'
require 'jrubyfx-fxmlloader'
#require 'dbi' 
#require 'dbd/jdbc'
require 'jdbc/sqlite3'


fxml_root File.dirname(__FILE__)

class Main < JRubyFX::Application
  def start(stage)
    with(stage, title: "sample", fxml: SimpleController ) do
    end.show
  end 
end

class SimpleController
  include JRubyFX::Controller
  fxml "hello.fxml"

  # fxmlの「onAction="#click_btn"」対応する
  on :click_btn do
    puts "pushed!!!"
    @b1_fx.text = "Pushed!!!"    # fxml1の「fx:id="view"」に対応する
  end 
end

Jdbc::SQLite3.load_driver
#databasefile = '\\yagyu_kanri\\kanri.db'
#url = "jdbc:sqlite:test.db"
#Java::org.sqlite.JDBC #initialize the driver
#connection = JavaLang::DriverManager.getConnection(url) #grab your connection

c = java.sql::DriverManager.getConnection(Jdbc::SQLite3.driver_name)

databasefile = 'test.db'
dbh = DBI.connect(
#  "DBI:Jdbc:sqlite:#{databasefile}",  # connection string
  "DBI:Jdbc:sqlite:#{databasefile}",  # connection string
  '',                                 # no username for sqlite3
  '',                                 # no password for sqlite3
  'driver' => 'org.sqlite.JDBC')      # need to set the driver



Main.launch
